// for when you don't specify quasar.conf.js > framework: 'all'
// import { Quasar } from 'quasar'
// import Vue from 'vue'
let lang = localStorage.getItem('lang')
if (!lang) {
  lang = 'zh-hk'
}
const routes = [
  /*
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { name: 'index1', path: '', component: () => import('pages/Index.vue'), meta: { requireAuth: true } },
      { name: 'login1', path: 'login', component: () => import('pages/auth/login.vue') },
      { name: 'register1', path: 'register', component: () => import('pages/auth/login.vue') },
      { name: 'secure1', path: 'secure', component: () => import('pages/secure.vue') },
      { name: 'logout1', path: 'logout', component: () => import('pages/auth/logout.vue') },
      { name: 'lang1', path: 'lang', component: () => import('pages/lang.vue') },
      { name: 'tour1', path: 'tour', component: () => import('pages/tour/tour.vue') },
      { name: 'draw1', path: 'draw', component: () => import('pages/tour/draw.vue') },
      { name: 'rank1', path: 'rank', component: () => import('pages/other/rank.vue') },
      { name: 'mygame1', path: 'mygame', component: () => import('pages/tour/mygame.vue') },
      { name: 'contact1', path: 'contact', component: () => import('pages/other/contact.vue') }
    ]
  },
  */
  { path: '/', redirect: '/' + lang + '/tour/list/1' },
  {
    path: '/:lang',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      // { name: 'index', path: '', component: () => import('pages/Index.vue'), meta: { requireAuth: true } },
      { name: 'login', path: 'login', component: () => import('pages/auth/login.vue') },
      { name: 'register', path: 'register', component: () => import('pages/auth/login.vue') },
      { name: 'secure', path: 'secure', component: () => import('pages/secure.vue') },
      { name: 'logout', path: 'logout', component: () => import('pages/auth/logout.vue') },
      { name: 'lang', path: 'lang', component: () => import('pages/lang.vue') },
      { path: 'tour', component: () => import('pages/tour/tour.vue') },
      { name: 'tourlist', path: 'tour/list/:page', props: true, component: () => import('pages/tour/tourlist.vue') },
      { name: 'tourinfo', path: 'tour/info/:tour_id', props: true, component: () => import('pages/tour/tourframe.vue') },
      { path: 'tour/draw', component: () => import('pages/tour/draw.vue') },
      { path: 'user', component: () => import('pages/user/user.vue') },
      { path: 'user/:uid', component: () => import('pages/user/userid.vue') },
      { path: 'user/:uid/:type', component: () => import('pages/user/userlink.vue') },
      { name: 'user', path: 'user/:uid/:type/:page', component: () => import('pages/user/user.vue') },
      { path: 'setting', component: () => import('pages/setting/setting.vue') },
      { path: 'setting/:type', component: () => import('pages/setting/settinglist.vue') },
      { name: 'setting', path: 'setting/:type', component: () => import('pages/setting/settinglist.vue') },
      { path: 'draw', component: () => import('pages/tour/draw.vue') },
      { path: 'draw/:tour_id', component: () => import('pages/tour/tourframe.vue') },
      { name: 'draw', path: 'draw/:tour_id/:divi', component: () => import('pages/tour/tourframe.vue') },
      { name: 'apply', path: 'tour/apply/:tour_id', component: () => import('pages/tour/tourframe.vue') },
      { path: 'rank', component: () => import('pages/other/rank.vue') },
      { path: 'rank/:mode', component: () => import('pages/other/ranklist.vue') },
      { name: 'rank', path: 'rank/:mode/:page', component: () => import('pages/other/ranklist.vue') },
      { name: 'schedule', path: 'member/schedule/:page', component: () => import('pages/member/schedule.vue') },
      { name: 'member', path: 'member', component: () => import('pages/member/schedule.vue') },
      { name: 'report', path: 'member/report/:type/:id', component: () => import('pages/member/report.vue') },
      { name: 'mygame', path: 'mygame', component: () => import('pages/tour/mygame.vue') },
      { name: 'contact', path: 'contact', component: () => import('pages/other/contact.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
