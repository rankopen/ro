import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import { Notify } from 'quasar'
import i18n from 'src/i18n/i18n'
import axios from 'axios'
Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: (to, from, savedPosition) => {
      if (to.name === 'user' && from.name === 'user') { // If it is user page, don't jump around
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    },
    routes,
    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeEach(async (to, from, next) => { // Before getting into Pages
    // Start :: axios interceptors
    // Set header to local storage JWT token
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('JWT_TOKEN')
    // Axios request interceptors
    axios.interceptors.request.use(
      config => {
        const token = localStorage.getItem('JWT_TOKEN')
        if (token) {
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        }
        return config
      },
      error => {
        return Promise.reject(error)
      }
    )
    // http response interceptors
    axios.interceptors.response.use(response => {
      if (response.data.code === 401) {
        // Token expired, login again
        // this.$q.notify({ message: '驗證失敗，請重新登入', position: this.$glo.notePos, color: this.$glo.noteColor })
        Notify.create({ message: i18n.t('tokenExpire'), position: Vue.prototype.$glo.notePos, color: Vue.prototype.$glo.noteColor, timeout: 2000, actions: [{ icon: 'close', color: 'white' }] })
        localStorage.clear()
        next('/' + i18n.locale + '/login')
      } else {
        if (response.data.jwt) {
          localStorage.setItem('JWT_TOKEN', response.data.jwt) // Refresh local storage token time
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.jwt // Refresh JWT token to header
        }
        return response // return the query return result
      }
    }, error => {
      return Promise.reject(error)
    })
    // End :: axios interceptors
    // const language = to.params.lang
    var curPath = to.path // Remove the lang from the path
    var truePath = '/' + curPath.split('/').slice(2).join('/') // Remove the lang from the path
    let language = curPath.split('/')[1] // Get the lang from the path
    // const language = to.params.lang
    Vue.prototype.$glo.activePath = curPath.split('/')[2] // Take the second slot of the path
    if (language === 'en-us') {
      i18n.locale = 'en-us'
    } else if (language === 'zh-hant') {
      i18n.locale = 'zh-hant'
    } else {
      i18n.locale = 'zh-hant'
    }
    // End :: Language Control
    const pubPage = [
      // /^\/$/,
      /^\/logout$/,
      /^\/secure$/,
      /^\/tour$/,
      /^\/tour\/.*$/,
      /^\/tourinfo\/.*$/,
      /^\/user$/,
      /^\/user\/.*$/,
      /^\/draw$/,
      /^\/draw\/.*$/,
      /^\/rank$/,
      /^\/rank\/.*$/,
      /^\/contact$/,
      /^\/login$/,
      /^\/lang$/,
      /^\/register$/,
      /^\/lang\/.*$/
      // /^\/.*$/
    ] // "/", "/login", "/lang/*" regular expression
    const authPub = pubPage.some(rx => rx.test(truePath)) // if the path matches the pubPage regular expression
    const loggedIn = localStorage.getItem('JWT_TOKEN') // Logic to see if there is token
    if (to.name !== 'login' && to.name !== 'logout') {
      console.log(to.name)
      Vue.prototype.$glo.prePath = truePath
    }
    if (authPub) { // if it is public pages
      if (to.name === 'login') { // if already loggedIn
        if (loggedIn) {
          Notify.create({ message: i18n.t('loggedIn'), position: 'top', color: 'cyan', timeout: 2000, actions: [{ icon: 'close', color: 'white' }] })
          next(from.path)
        } else {
          next()
        }
      } else {
        next() // Proceed
      }
    } else if (loggedIn === 'undefined') {
      Notify.create({ message: i18n.t('plsLogin'), position: Vue.prototype.$glo.notePos, color: Vue.prototype.$glo.noteColor, timeout: 2000, actions: [{ icon: 'close', color: 'white' }] }) // Notice need log in
      // Vue.prototype.$glo.prePath = truePath // Set previous path into global value
      next('/' + i18n.locale + '/login') // Go to login page
    } else if (loggedIn) { // if there is JWT_TOKEN and logged in
      if (truePath === 'login') { // if path = login
        Notify.create({ message: i18n.t('loggedIn'), position: 'top', color: 'cyan', timeout: 2000, actions: [{ icon: 'close', color: 'white' }] })
        next() // Proceed this page
      } else { // If it is other pages
        next() // Proceed
      }
    } else { // If there is no token and auth required, need to log in
      if (truePath === 'login') { // If the path is '/login', proceed
        next() // Proceed this page
      } else if (truePath === 'logout') { // If the path is '/logout', proceed
        next() // Proceed this page
      } else { // If it is other path
        Notify.create({ message: i18n.t('plsLogin'), position: Vue.prototype.$glo.notePos, color: Vue.prototype.$glo.noteColor, timeout: 2000, actions: [{ icon: 'close', color: 'white' }] }) // Notice need log in
        // Vue.prototype.$glo.prePath = truePath // Set previous path into global value
        next('/' + i18n.locale + '/login') // Go to login page
        // next({ path: '/login', query: { from: truePath } })
      }
    }
    console.log(Vue.prototype.$glo.prePath)
  })
  return Router
}

/* Backup code
// Start :: Language Control
// Now you need to add your authentication logic here, like calling an API endpoint
// localStorage.setItem('JWT_TOKEN', 'eyJ0eXAi')
// const publicPages = ['/login', '/', '/lang'] // Public Pages, no auth required
// const privatePages = ['/secure', '/', '/lang'] // Private Pages, auth required
// const authRequired = publicPages.includes(to.path) // Logic for no auth poth
// const authRequired = to.path.match(/^\/lang\/.*$/)

// If it is not english, switch to default chinese
if (language !== 'en-us' && i18n.locale !== 'en-us') { // if it is not state to be english, If it is incorrect language, redirect to zh-hant
  console.log('111')
  i18n.locale = 'zh-hant' // set to Default language
} else if (language !== 'en-us' && i18n.locale === 'en-us') {
  console.log('222')
  i18n.locale = 'en-us'
  next('/en-us' + truePath)
} else if (language === 'en-us') {
  i18n.locale = 'en-us'
} else { // There is a language in the link
  console.log('333')
  i18n.locale = 'zh-hant'
}
*/
