/*
import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

export default async ({ app, Vue }) => {
  Vue.use(VueI18n)
  // Set i18n instance on app
  app.i18n = new VueI18n({
    locale: 'en-us',
    fallbackLocale: 'en-us',
    messages
  })
}

//
Vue.use(VueI18n)
export const i18n = new VueI18n({
  locale: 'en-us',
  fallbackLocale: 'en-us',
  messages
})
*/
import i18n from 'src/i18n/i18n'

export default ({ app, Vue }) => {
  app.i18n = i18n
}
