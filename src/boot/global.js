import axios from 'axios'

const gloModule = {
  userInfo: [],
  prePath: window.location.pathname, // Previous Path for redirection after login
  activePath: '', // Current path for menu active
  notePos: 'top', // Default notify position
  noteColor: 'cyan', // Default notify color
  locale: 'zh-hant',
  apiLink: 'https://rankopen.com/plugin.php?id=api',
  headCode: '', // This is global sub header code, can use html
  showToken () {
    console.log(localStorage.getItem('JWT_TOKEN'))
    console.log(axios.defaults.headers.common['Authorization'])
  },
  removeToken () {
    localStorage.removeItem('JWT_TOKEN')
    this.$axios.defaults.headers.common['Authorization'] = ''
    console.log('Token Removed')
    console.log(localStorage.getItem('JWT_TOKEN'))
  },
  changeLang (lang, path) {
    var pathLang = path.split('/') // getLang[1] equlas to the lang value
    if (!['zh-hant', 'en-us'].includes(pathLang[1])) { // if there's no language in the path
      if (lang === 'zh-hant') {
        return path // If it is default language, return only path with no lang
      } else {
        return lang + path // go to the path with new lang
      }
    } else { // if there is already a lang in the path
      var removePath = path.split('/').slice(2).join('/') // get the path after the second slashS
      if (lang === 'zh-hant') {
        return '/' + lang + '/' + removePath // If it is default language, return only path with no lang
      } else {
        return '/' + lang + '/' + removePath // go to the path with new lang
      }
    }
  }
}

export default async ({ app, router, store, Vue }) => {
  Vue.prototype.$glo = gloModule
}
