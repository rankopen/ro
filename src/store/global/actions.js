/*
export function someAction (context) {
}
*/
import Vue from 'vue'
import axios from 'axios'
import i18n from 'src/i18n/i18n'
export function logoutAction (context) {
  context.commit('logout') // Run mutations
}

export function testLink (context) {
  // return this.$glo.apiLink // Run mutations
  axios.post(Vue.prototype.$glo.apiLink + '&mode=pub&tab=info', JSON.stringify({ lang: i18n.locale }))
    .then((response) => {
      console.log((response.data))
      context.commit('testMuta', decodeURI(response.data.place))
    })
}
export function testLang (lang) {
  this.$axios.post('https://rankopen.com/plugin.php?id=api&mode=pub&tab=login', JSON.stringify({ username: encodeURI(this.form.username), password: this.form.password }))
  // this.$axios.get('https://rankopen.com/testapi.php')
    .then((response) => {
      if (response.data === 'fail' || !response.data) {
        this.$q.notify({ message: '登入失敗', position: this.$glo.notePos, color: this.$glo.noteColor })
      } else {
        localStorage.setItem('JWT_TOKEN', response.data) // Save returned JWT token to local storage
        this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data // Save returned JWT token to header
        this.$q.notify({ message: '成功登入', position: this.$glo.notePos, color: this.$glo.noteColor })
        this.$router.replace(this.$glo.prePath) // Go to previous path saved at global variables
        console.log(decodeURI(response.data))
      }
      // alert(localStorage.getItem('JWT_TOKEN'))
      // this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('JWT_TOKEN')
      // this.$axios.defaults.headers.common['Authorization'] = 'test login.vue'
      // this.$router.go(-1) // go to previous page
      // console.log(this.$axios.defaults.headers.common['Authorization'])
      // this.$q.notify(response.data)
    })
    .catch(() => {
      this.$q.notify({ message: '驗證失敗', position: this.$glo.notePos, color: this.$glo.noteColor })
    })
}
