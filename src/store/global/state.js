export default {
  uid: 555,
  langAry: { 'en-us': 'English (US)', 'zh-hant': '繁體中文' },
  token: localStorage.getItem('JWT_TOKEN') || '',
  status: true,
  testState: ''
}
